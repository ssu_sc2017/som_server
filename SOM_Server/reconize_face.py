import http.client
import http.client, urllib.request, urllib.parse, urllib.error
import json, requests


def add_face_to_list(piciture=None):
    headers = {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': '6e884275dabd48868621d8d35842fed5',
    }
    data = open(piciture, "rb").read(1000000)
    try:
        conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
        conn.request("POST", "/face/v1.0/facelists/som_user/persistedFaces", data, headers)
        response = conn.getresponse()
        json_string = response.read().decode('utf-8')
        json_obj = json.loads(json_string)
        face_id = json_obj['persistedFaceId']
        conn.close()
        if not face_id:
            print("CAN NOT FIND FACE IN PICTURE")
            return None
        else:
            print("ADD FACE TO LIST SUCCESS")
            print("faceId : ", face_id)
            return face_id
    except Exception as e:
        print(e)
        return None


def reco_face(face_id=None):
    headers = {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': '6e884275dabd48868621d8d35842fed5'
    }
    params = {
        "faceId": face_id,
        "faceListId": 'som_user',
    }
    try:
        url = "https://westus.api.cognitive.microsoft.com/face/v1.0/findsimilars"
        data = requests.post(url, headers=headers, json=params)
        confidence = data.json()[0]['confidence']
        persistedFaceId = data.json()[0]['persistedFaceId']
        if not confidence:
            return None
        return persistedFaceId, confidence

    except Exception as e:
        print("NO MATCH")
        return None


def get_faceId(picture):
    headers = {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': '6e884275dabd48868621d8d35842fed5',
    }
    params = urllib.parse.urlencode({
        'returnFaceId': 'true',
    })
    data = open(picture, "rb").read(1000000)
    try:
        conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
        conn.request("POST", "/face/v1.0/detect?%s" % params, data, headers)
        response = conn.getresponse()
        json_string = response.read().decode('utf-8')
        json_obj = json.loads(json_string)
        face_id = json_obj[0]['faceId']
        conn.close()
        return face_id
    except Exception as e:
        print(e)


if __name__ == '__main__':
    # persistedFaceId = add_face_to_list(piciture="hs.jpg")

    face_id = get_faceId(picture="hs.jpg")
    persistedFaceId, confidence = reco_face(face_id=face_id)
    if persistedFaceId:
        print("persistedFaceId : %s with confidence : %s" % (persistedFaceId, confidence))
    else:
        print("NO MATCH")
"""SOM_Server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
import SOM_Server.apis as apis

urlpatterns = [
    url(r'^api/user$', apis.manage_user),
    url(r'^api/user/(?P<option>\w+)$', apis.manage_user),
    url(r'^api/command$', apis.manage_command),
    url(r'^api/command/(?P<option>\w+)$', apis.manage_command),
    url(r'^api/request$', apis.manage_request),
    url(r'^api/request/(?P<option>\w+)$', apis.manage_request),
]

# settings.DEBUG 가 False 면 작동 안함
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
import datetime
import re
import requests
import random
import urllib

from pprint import pprint
# sentence = "다다음달 13일 수강신청"
# date = datetime.date.today()
# year, month, day = date.year, date.month, date.day
# print(year, month, day)
#
# if re.findall(r"\d*\d*\d*\d*년", sentence):
#     year = int(re.sub(r"[^0-9]", '', re.findall(r"\d*\d*\d*\d*년", sentence)[0]))
# if re.findall(r"\d*\d*월", sentence):
#     month = int(re.sub(r"[^0-9]", '', re.findall(r"\d*\d*월", sentence)[0]))
# elif re.findall(r"다다음 달", sentence) or re.findall(r"다다음달", sentence):
#     tmp = date + datetime.timedelta(60)
#     month = tmp.month
# elif re.findall(r"다음 달", sentence) or re.findall(r"다음달", sentence):
#     tmp = date + datetime.timedelta(30)
#     month = tmp.month
# if re.findall(r"\d*\d*일", sentence):
#     day = int(re.sub(r"[^0-9]", '', re.findall(r"\d*\d*일", sentence)[0]))
# print("날짜 : ", year, month, day)

#######################################################################################################################

# from bs4 import BeautifulSoup
# import urllib.request
#
# URL = 'https://www.youtube.com/results?search_query='
#
#
# def get_music_url(query):
#     url_id = None
#     url_id_list = []
#     for q in query:
#         source_code_from_url = urllib.request.urlopen(URL + q.replace(" ", "+"))
#         soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
#         for page in soup.find_all('h3', {"class": "yt-lockup-title "}):
#             url_id = page.find('a')['href']
#             if 'watch?' in url_id:
#                 url_id_list.append(url_id[9:])
#                 break
#     return url_id_list
#
# def get_music_list_from_mood(mood=1, year=None):
#     arousal = 1000000
#     mood *= 100000
#     date50, date60, date70, date80, date90, date00, date10 = True, True, True, True, True, True, True
#     if year:
#         if year == 1950 or year == 50:
#             date50 = True
#             date60, date70, date80, date90, date00, date10 = False, False, False, False, False, False
#         if year == 1960 or year == 60:
#             date60 = True
#             date50, date70, date80, date90, date00, date10 = False, False, False, False, False, False
#         if year == 1970 or year == 70:
#             date70 = True
#             date60, date50, date80, date90, date00, date10 = False, False, False, False, False, False
#         if year == 1980 or year == 80:
#             date80 = True
#             date60, date70, date50, date90, date00, date10 = False, False, False, False, False, False
#         if year == 1990 or year == 90:
#             date90 = True
#             date60, date70, date80, date50, date00, date10 = False, False, False, False, False, False
#         if year == 2000 or year == 00:
#             date00 = True
#             date60, date70, date80, date90, date50, date10 = False, False, False, False, False, False
#         if year == 2010 or year == 10:
#             date10 = True
#             date60, date70, date80, date90, date00, date50 = False, False, False, False, False, False
#
#     URL = 'http://musicovery.com/api/V3/playlist.php'
#     params = {'fct': 'getfrommood', 'apikey': 'w0g61dz3', 'popularitymin': '90', 'trackvalence': mood, 'date50': date50,
#               'date60': date60, 'date70': date70, 'date80': date80, 'date90': date90, 'date00': date00,
#               'date10': date10, 'trackarousal': arousal - mood, 'resultsnumber': '5', 'listenercountry': 'es'}
#     res = requests.get(URL, params=params)
#     reco_list = []
#     for s in res.text.split('!'):
#         to = s.find("/title")
#         to2 = s.find("/name")
#         if to > 0:
#             tmp = s[7:to]
#             title = re.sub(r"[^a-zA-Z ]", '', tmp)
#         if to2 > 0:
#             tmp = s[7:to2]
#             artist = re.sub(r"[^a-zA-Z ]", '', tmp)
#             reco_list.append(title + ' ' + artist)
#
#     return reco_list
#
# if __name__ == '__main__':
#     sentence = '최신 노래중에 신나는 노래 추천'
#     if '추천' in sentence:
#         mood = 1
#         year = None
#         if '신나' in sentence:
#             mood = random.randint(0, 2)
#         if '우울' in sentence or '분위기' in sentence:
#             mood = random.randint(7, 10)
#         if re.findall(r"\d*\d*\d*\d*년", sentence):
#             year = int(re.sub(r"[^0-9]", '', re.findall(r"\d*\d*\d*\d*년", sentence)[0]))
#         elif '요즘' in sentence or '최신' in sentence:
#             year = 2010
#         option_ = get_music_list_from_mood(mood=mood, year=year)
#     print('mood : ', mood, 'year : ', year)
#     url_id_list = get_music_url(option_)
#     print(url_id_list)


#######################################################################################################################

# from bs4 import BeautifulSoup
# import urllib.request
#
# URL = 'http://news.naver.com/main/main.nhn?mode=LSD&mid=shm&sid1='
#
#
# def get_news_list(URL):
#     news_list = []
#     news_header = []
#     source_code_from_url = urllib.request.urlopen(URL)
#     soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
#     for page in soup.find_all('div', id='main_content'):
#         main = page.find('div').find_all('div')
#         for m in main:
#             for news in m.find_all('dl'):
#                 if news.find('a') is not None:
#                     news_list.append(news.find('a')['href'])
#                     tmp = news.find('a', text=True).find(text=True)
#                     news_header.append(re.sub('[\n\r\t\{\}\[\]\/?,;:|\)*~`!^\-_+<>\#$%&\\\=\(\'\"]', '', tmp))
#     return news_list, news_header
#
#
# def get_news_text(url):
#     tmp1 = ''
#     tmp2 = ''
#     txt = ''
#     source_code_from_url = urllib.request.urlopen(url)
#     soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
#     for body_text in soup.find_all('div', id='articleBodyContents'):
#         for te in body_text.find_all(text=True):
#             tmp1 += te
#     if '@' in tmp1:
#         tmp1 = re.sub('[\n\r\t\{\}\[\]\/?,;:|\)*~`!^\-_+<>\#$%&\\\=\(\'\"]', '', tmp1).split('flashremoveCallback')[1].split('@', 5)[:-1]
#     else:
#         tmp1 = re.sub('[\n\r\t\{\}\[\]\/?,;:|\)*~`!^\-_+<>\#$%&\\\=\(\'\"]', '', tmp1).split('flashremoveCallback')[1]
#     for t in tmp1:
#         tmp2 += t
#     tmp1 = tmp2.split('.')[:-1]
#     for t in tmp1:
#         txt += t
#     return txt
#
# if __name__ == '__main__':
#     area = 103
#     option_ = []
#     say_ = []
#     tmp = []
#     news_url, news_header = get_news_list(URL + str(area))
#     option_.append(news_url)
#     option_.append(news_header)
#     for url in news_url:
#         say_.append(get_news_text(url))
#     print(option_)
#     print(say_)

#######################################################################################################################

# import http.client
# import http.client, urllib.request, urllib.parse, urllib.error
# import json, requests
#
#
# def add_face_to_list(piciture=None):
#     headers = {
#         'Content-Type': 'application/octet-stream',
#         'Ocp-Apim-Subscription-Key': '6e884275dabd48868621d8d35842fed5',
#     }
#     data = open(piciture, "rb").read(1000000)
#     try:
#         conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
#         conn.request("POST", "/face/v1.0/facelists/som_user/persistedFaces", data, headers)
#         response = conn.getresponse()
#         json_string = response.read().decode('utf-8')
#         json_obj = json.loads(json_string)
#         face_id = json_obj['persistedFaceId']
#         conn.close()
#         if not face_id:
#             print("CAN NOT FIND FACE IN PICTURE")
#             return None
#         else:
#             print("ADD FACE TO LIST SUCCESS")
#             print("faceId : ", face_id)
#             return face_id
#     except Exception as e:
#         print(e)
#         return None
#
#
# def reco_face(face_id=None):
#     headers = {
#         'Content-Type': 'application/json',
#         'Ocp-Apim-Subscription-Key': '6e884275dabd48868621d8d35842fed5'
#     }
#     params = {
#         "faceId": face_id,
#         "faceListId": 'som_user',
#     }
#     try:
#         url = "https://westus.api.cognitive.microsoft.com/face/v1.0/findsimilars"
#         data = requests.post(url, headers=headers, json=params)
#         confidence = data.json()[0]['confidence']
#         persistedFaceId = data.json()[0]['persistedFaceId']
#         if not confidence:
#             return None
#         return persistedFaceId, confidence
#
#     except Exception as e:
#         print("NO MATCH")
#         return None
#
#
# def get_faceId(picture):
#     headers = {
#         'Content-Type': 'application/octet-stream',
#         'Ocp-Apim-Subscription-Key': '6e884275dabd48868621d8d35842fed5',
#     }
#     params = urllib.parse.urlencode({
#         'returnFaceId': 'true',
#     })
#     data = open(picture, "rb").read(1000000)
#     try:
#         conn = http.client.HTTPSConnection('westus.api.cognitive.microsoft.com')
#         conn.request("POST", "/face/v1.0/detect?%s" % params, data, headers)
#         response = conn.getresponse()
#         json_string = response.read().decode('utf-8')
#         json_obj = json.loads(json_string)
#         face_id = json_obj[0]['faceId']
#         conn.close()
#         return face_id
#     except Exception as e:
#         print(e)
#
#
# if __name__ == '__main__':
#     # persistedFaceId = add_face_to_list(piciture="hs.jpg")
#
#     face_id = get_faceId(picture="hs.jpg")
#     persistedFaceId, confidence = reco_face(face_id=face_id)
#     if persistedFaceId:
#         print("persistedFaceId : %s with confidence : %s" % (persistedFaceId, confidence))
#     else:
#         print("NO MATCH")

# from bs4 import BeautifulSoup
# import urllib.request, urllib.parse
#
# URL = 'http://www.10000recipe.com'
#
#
# def get_url_list(keyword):
#     url_list = []
#     params = urllib.parse.urlencode({
#         'q': keyword,
#     })
#     source_code_from_url = urllib.request.urlopen(URL + "/recipe/list.html?" + params)
#     soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
#     if soup.find_all('div', {'class': 'result_none'}):
#         return None
#     for page in soup.find_all('div', {"class": "col-xs-4"}):
#         url_list.append(page.find('a')['href'])
#     return url_list
#
#
# def get_recipe(url):
#     step_text_list = []
#     step_img_src_list = []
#     try:
#         source_code_from_url = urllib.request.urlopen(URL + url)
#         soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
#         for step in range(0, 20):
#             for page in soup.find_all('div', {"id": "stepDiv"+str(step)}):
#                 step_text_list.append(page.text)
#                 if page.find_all('div', {"id": "stepimg"+str(step)}):
#                     step_img_src_list.append(page.find_all('div', {"id": "stepimg" + str(step)})[0].find_all("img")[0].get('src'))
#         return step_text_list, step_img_src_list
#     except:
#         return step_text_list, step_img_src_list
#
# if __name__ == '__main__':
#     keyword = "야채 햄"
#     url_list = get_url_list(keyword)
#     if not url_list:
#         print('\''+keyword+"\'", "에 대한 검색결과가 없습니다.")
#     else:
#         for i in url_list:
#             step_text_list, step_img_src_list = get_recipe(i)
#             for img, txt in zip(step_img_src_list, step_text_list):
#                 print(txt, " : ", img)

from SOM_Server.command_analyzer_dir.training import Trainer

# if __name__ == '__main__':
    # trainer = Trainer(filename='myTrainset.txt', model_name='model')    # 메인 model
    # trainer = Trainer(filename='userdata.txt', model_name='selfModel')    # self 모델
    # trainer.do_train()
    # model = gensim.models.Word2Vec.load('selfModel')
    # print("시 : ", model.most_similar(positive="김지훈"))

from bs4 import BeautifulSoup
import tensorflow as tf
import numpy as np

xy = np.loadtxt('data-03-diabetes.csv', delimiter=',', dtype=np.float32)
x_data = xy[:, 0:-1]
y_data = xy[:, [-1]]

X = tf.placeholder(tf.float32, shape=[None, 8])
Y = tf.placeholder(tf.float32, shape=[None, 1])
W = tf.Variable(tf.random_normal([8, 1], name='weight'))    # 2개 들어와서 1개 나간다
b = tf.Variable(tf.random_normal([1]), name='bias')     # 나가는 값과 항상 같음 1!

# Hypothesis using sigmoid: tf.div(1., 1. + tf.exp(tf.matmul(X, W) + b))
hypo = tf.sigmoid(tf.matmul(X, W) + b)

cost = -tf.reduce_mean(Y * tf.log(hypo) + (1 - Y) * tf.log(1 - hypo))

train = tf.train.GradientDescentOptimizer(learning_rate=0.01).minimize(cost)

predicted = tf.cast(hypo > 0.5, dtype=tf.float32)   # 0.5 보다 크면 1 작으면 0
accuracy = tf.reduce_mean(tf.cast(tf.equal(predicted, Y), dtype=tf.float32))    # 정확도

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    for step in range(10001):
        cost_val, _ = sess.run([cost, train], feed_dict={X: x_data, Y: y_data})
        if step % 200 == 0:
            print(step, cost_val)

    h, c, a = sess.run([hypo, predicted, accuracy], feed_dict={X:x_data, Y:y_data})

print("h : ", h, '\n', "c : ", c, '\n', "a : ", a)


ALLOWED_EXTEND = ['.jpg', '.JPG', '.png', '.PNG', '.gif', '.GIF', '.jpeg']

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')


def crop_face(path):
    for (path, dir, files) in os.walk(path):
        #if dir:
        #    continue
        if '_cut' in path:
            continue
        if not os.path.exists('test_images/' + path + '_cut'):
            os.makedirs('test_images/' + path + '_cut')
        for filename in files:
            ext = os.path.splitext(filename)[-1]
            if ext in ALLOWED_EXTEND:
                img = cv2.imread(path + '/' + filename)
                try:
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

                except:
                    continue
                faces = face_cascade.detectMultiScale(
                    gray,
                    scaleFactor=1.5,
                    minNeighbors=3,
                    minSize=(30, 30)
                )

                # print('found {0} faces'.format(len(faces)))
                if len(faces) == 0:
                    continue
                for idx, face in enumerate(faces):
                    roi_gray = img[face[1] - 20:face[1] + face[3] + 20,
                               face[0] - 20:face[0] + face[2] + 20]
                    cv2.imwrite('test_images/' + path + '_cut/' + str(idx) + '_' + filename , roi_gray)
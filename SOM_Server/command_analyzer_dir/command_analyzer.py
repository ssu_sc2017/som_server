from SOM_Server.command_analyzer_dir.korean_command_analyzer import CommandAnalyzer
from SOM_Server.command_analyzer_dir.training import Trainer
import re
import random
from SOM_Server.command_analyzer_dir.korean_command_analyzer import command_list
import datetime


cl = CommandAnalyzer()
trainer = Trainer(filename='.txt', model_name='selfModel')

sentences = ['학습', '김준 바보 멍청이']
isLearning = False
similarity = -1
command_ = -1
option_ = []
say_ = []
for sentence in sentences:
    for key in command_list.keys():
        if re.findall(command_list[key], sentence):
            tmp = 1.0
        else:
            tmp = cl.similarity(command_list[key], sentence)
            print("Command : ", command_list[key], tmp)
        if similarity < tmp:
            similarity = tmp
            command_ = command_list[key]

    if isLearning:
        for i in range(100):
            trainer.write_data(sentence)
        isLearning = False

    if similarity < 0.6:
        print("Command is not Predefined")
    else:
        if command_ == '학습':
            isLearning = True
        if command_ == '노래':
            if '트로트' in sentence:
                option_ = ['트로트']
            if '아이돌' in sentence:
                option_ = ['아이돌']
            if '힙합' in sentence:
                option_ = ['힙합']
            if '랩' in sentence:
                option_ = ['랩']
        elif command_ == '알람':
            hour, min = 0, 0
            if re.findall(r"\d*\d*시", sentence) != []:
                hour = int(re.sub(r"[^0-9]", '', re.findall(r"\d시", sentence)[0]))
            if re.findall(r"\d*\d*분", sentence) != []:
                min = int(re.sub(r"[^0-9]", '', re.findall(r"\d*\d*분", sentence)[0]))
            elif re.findall(r"반에", sentence) != []:
                min = int(30)
            elif re.findall(r"오후", sentence) != []:
                hour = (hour % 12) + 12
            if hour != 0 and min != 0:
                option_ = [hour, min]
            print("%d 시 %d 분 알람 입니다." % (hour, min))
        elif command_ == '욕':
            say = ['그런 무례한 표현은 삼가주세요', '반사', '입이 거치시네요', '너도요']
            say_ = [say[random.randint(0, 3)]]
        elif command_ == '경쟁자':
            say = ['아 그 친구요? 고등학교 때 제 빵 사오던 친구인데', '누구요?', '그런 사소한 이름은 기억하지 않습니다']
            say_ = [say[random.randint(0, 2)]]
        elif command_ == '검색':
            search_list = cl.tokenizer.nouns(sentence)
            for search in search_list:
                if search == '검색':
                    continue
                else:
                    option_.append(search)
        elif command_ == '날씨' or '일정':
            date = datetime.date.today()
            if re.findall(r"오늘", sentence) != []:
                say_ = '오늘'
            elif re.findall(r"내일", sentence) != []:
                say_ = '내일'
            if re.findall(r"요일", sentence) != []:
                if re.findall(r"월요일", sentence) != []:
                    wanted_yoil = 1
                if re.findall(r"화요일", sentence) != []:
                    wanted_yoil = 2
                if re.findall(r"수요일", sentence) != []:
                    wanted_yoil = 3
                if re.findall(r"목요일", sentence) != []:
                    wanted_yoil = 4
                if re.findall(r"금요일", sentence) != []:
                    wanted_yoil = 5
                if re.findall(r"토요일", sentence) != []:
                    wanted_yoil = 6
                if re.findall(r"일요일", sentence) != []:
                    wanted_yoil = 7
                origin_yoil = date.isoweekday()
                if wanted_yoil >= origin_yoil:
                    def_ = wanted_yoil - origin_yoil
                    if date.day + def_ > 30 and date.month in (4, 6, 9, 11):
                        date = date.replace(month=date.month+1, day=(date.day+def_) - 30)
                    elif date.day + def_ > 31 and date.month in (1, 3, 5, 7, 8, 10, 12):
                        date = date.replace(month=date.month + 1, day=(date.day + def_) - 31)
                    elif date.day + def_ > 28 and date.month == 2:
                        date = date.replace(month=date.month + 1, day=(date.day + def_) - 28)
                    else:
                        date = date = date.replace(day=date.day + def_)
                    if re.findall(r"다음", sentence) != []:
                        if date.day + 7 > 30 and date.month in (4, 6, 9, 11):
                            date = date.replace(month=date.month + 1, day=(date.day + 7) - 30)
                        elif date.day + 7 > 31 and date.month in (1, 3, 5, 7, 8, 10, 12):
                            date = date.replace(month=date.month + 1, day=(date.day + 7) - 31)
                        elif date.day + 7 > 28 and date.month == 2:
                            date = date.replace(month=date.month + 1, day=(date.day + 7) - 28)
                        else:
                            date = date.replace(day=date.day + 7)
                else:
                    def_ = 7 - (origin_yoil - wanted_yoil)
                    if date.day + def_ > 30 and date.month in (4, 6, 9, 11):
                        date = date.replace(month=date.month+1, day=(date.day+def_) - 30)
                    elif date.day + def_ > 31 and date.month in (1, 3, 5, 7, 8, 10, 12):
                        date = date.replace(month=date.month + 1, day=(date.day + def_) - 31)
                    elif date.day + def_ > 28 and date.month == 2:
                        date = date.replace(month=date.month + 1, day=(date.day + def_) - 28)
                    else:
                        date = date = date.replace(day=date.day + def_)

                print("date : ", date)
        else:
            print("The Most Similarity command is \"%s\" and Similarity is %.3f" % (command_, similarity))
        break



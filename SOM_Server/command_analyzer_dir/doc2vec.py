from konlpy.tag import Twitter
from collections import namedtuple
import pickle
from gensim.models import doc2vec

def tokenize(doc):
    return ['/'.join(t) for t in pos_tagger.pos(doc, norm=True, stem=True)]

def read_data(filename):
    with open(filename, 'r', encoding="utf-8") as f:
        data = [line.split('\t') for line in f.read().splitlines()]
        data = data[1:]
    return data

pos_tagger = Twitter()

train_data = read_data('1.txt')
train_docs = [(tokenize(row[0])) for row in train_data]
with open('wiki_1', 'wb') as f:
    pickle.dump(train_docs, f)

print(train_docs)
# TaggedDocument = namedtuple('TaggedDocument', 'words tags')

# with open('train', 'rb') as f:
#     train_docs = pickle.load(f)
# print(train_docs)
# print(train_docs)

    # 여기서는 15만개 training documents 전부 사용함
# tagged_train_docs = [TaggedDocument(d, [0]) for d in train_docs]
# print(tagged_train_docs)
# doc_vectorizer = doc2vec.Doc2Vec(size=300, alpha=0.025, min_alpha=0.025, seed=1234)
# doc_vectorizer.build_vocab(tagged_train_docs)

# Train document vectors!
# for epoch in range(10):
#     doc_vectorizer.train(tagged_train_docs)
#     doc_vectorizer.alpha -= 0.002  # decrease the learning rate
#     doc_vectorizer.min_alpha = doc_vectorizer.alpha  # fix the learning rate, no decay
# To save
# doc_vectorizer.save('train_wiki.model')
# doc_vectorizer = doc2vec.Doc2Vec.load('train_v0.model')
# doc_vectorizer = doc2vec.Doc2Vec.load('train_v0.model')
# print(doc_vectorizer.most_similar('불/Noun'))

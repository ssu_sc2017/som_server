from __future__ import division
import numpy as np
import gensim
import konlpy
from SOM_Server.settings import COMMAND_DIR
import os

ALPHA = 0.2
BETA = 0.45
ETA = 0.4
PHI = 0.80
DELTA = 0.90

N = 0
help_list = dict()
help_list['0'] = "헬로우 솜 - 솜을 깨우는 명령어"
help_list['1'] = "불 켜 - 등록된 불을 키는 명령어"
help_list['2'] = "불 꺼 - 등록된 불을 끄는 명령어"
help_list['3'] = "검색 - 네이버 또는 구글을 통해 특정 키워드를 검색하는 명령어"
help_list['4'] = "날씨 - 해당 지역의 날씨를 알려주는 명령어"
help_list['5'] = "시간 -  현재 시간을 알려주는 명령어"
help_list['6'] = "알람 - 특정 시간에 알림을 주는 명령어"
help_list['7'] = "욕 - 해보세요 한번"
help_list['8'] = "일정 - 특정 날짜에 할 일을 등록 해주는 명령어"
help_list['9'] = "경쟁자 - 그런게 있나 싶네요"
help_list['10'] = "환율 - 달러, 엔화, 유로, 위안 환율을 알려주는 명령어"
help_list['11'] = "메모 - 하고 싶은 말을 저장해 놓는 명령어"
help_list['12'] = "노래 - 음악을 듣거나 추천해주는 명령어"
help_list['13'] = "헬프 - 도와줘요"
help_list['14'] = "학습 - 특정 문장 또는 단어를 학습 시켜주는 명령어"
help_list['15'] = "타이머 - 특정 시간을 재주는 명령어"
help_list['16'] = "뉴스 - 뉴스를 보여주는 명령어"
help_list['17'] = "요리 - 부족한 널 내가 도와줄게"

command_list = dict()
command_list['0'] = "헬로우솜"
command_list['1'] = "불켜"
command_list['2'] = "불꺼"
command_list['3'] = "검색"
command_list['4'] = "날씨"
command_list['5'] = '시간'
command_list['6'] = '알람'
command_list['7'] = '욕'
command_list['8'] = '일정'
command_list['9'] = '경쟁자'
command_list['10'] = '환율'
command_list['11'] = '메모'
command_list['12'] = '노래'
command_list['13'] = '헬프'
command_list['14'] = '학습'
command_list['15'] = '타이머'
command_list['16'] = '뉴스'
command_list['17'] = '요리'
command_list['18'] = '로그인'
command_list['19'] = '얼굴등록'
command_list['20'] = '온습도'
command_list['21'] = '전등'
command_list['22'] = '로그아웃'


class CommandAnalyzer:

    def __init__(self):
        self.tokenizer = konlpy.tag.Twitter()
        self.word_vector = gensim.models.Word2Vec.load(os.path.join(COMMAND_DIR, 'dataset' + '\\' + 'model'))

    def tokenize(self, doc):
        result = []
        for t in self.tokenizer.pos(doc):
            if t[1] == 'Noun' or t[1] == 'Verb' or t[1] == 'Adjective' or t[0] == '몇':
                result.append(t[0])
            else:
                continue
        return result


    ######################### sentence similarity ##########################

    def most_similar_word(self, word, word_set):
        max_sim = -1.0
        sim_word = ""
        for ref_word in word_set:
            sim = self.get_most_similarity(word, ref_word)
            if sim > max_sim:
                max_sim = sim
                sim_word = ref_word
        return sim_word, max_sim

    def get_most_similarity(self, word, ref):
        max_sim = -1.0
        synsets_1 = self.word_vector.most_similar(positive=[word], topn=5)
        synsets_2 = self.word_vector.most_similar(positive=[ref], topn=5)
        if len(synsets_1) == 0 or len(synsets_2) == 0:
            return -1
        for syn_1 in synsets_1:
            for syn_2 in synsets_2:
                sim = self.word_vector.similarity(syn_1[0], syn_2[0])
                if sim > max_sim:
                    max_sim = sim
        return max_sim

    def semantic_vector(self, words, joint_words):
        sent_set = set(words)
        semvec = np.zeros(len(joint_words))
        i = 0
        for joint_word in joint_words:
            if joint_word in sent_set:
                # if word in union exists in the sentence, s(i) = 1 (unnormalized)
                semvec[i] = 1.0
            else:
                # find the most similar word in the joint set and set the sim value
                sim_word, max_sim = self.most_similar_word(joint_word, sent_set)
                semvec[i] = PHI if max_sim > PHI else 0.0
            i = i + 1
        return semvec

    def semantic_similarity(self, sentence_1, sentence_2):
        words_1 = self.tokenize(sentence_1)
        words_2 = self.tokenize(sentence_2)
        joint_words = list(set(list(words_1)).union(set(list(words_2))))
        vec_1 = self.semantic_vector(words_1, joint_words)
        vec_2 = self.semantic_vector(words_2, joint_words)
        return np.dot(vec_1, vec_2.T) / (np.linalg.norm(vec_1) * np.linalg.norm(vec_2))


    ######################### word order similarity ##########################

    def word_order_vector(self, words, joint_words, windex):
        wovec = np.zeros(len(joint_words))
        i = 0
        wordset = set(words)
        for joint_word in joint_words:
            if joint_word in wordset:
                # word in joint_words found in sentence, just populate the index
                wovec[i] = windex[joint_word]
            else:
                # word not in joint_words, find most similar word and populate
                # word_vector with the thresholded similarity
                sim_word, max_sim = self.most_similar_word(joint_word, wordset)
                if max_sim > ETA:
                    wovec[i] = windex[sim_word]
                else:
                    wovec[i] = 0
            i = i + 1
        return wovec

    def word_order_similarity(self, sentence_1, sentence_2):
        words_1 = self.tokenize(sentence_1)
        words_2 = self.tokenize(sentence_2)
        words_1 = sorted(words_1)
        words_2 = sorted(words_2)
        joint_words = list(set(words_1).union(set(words_2)))
        windex = {x[1]: x[0] for x in enumerate(joint_words)}
        r1 = self.word_order_vector(words_1, joint_words, windex)
        r2 = self.word_order_vector(words_2, joint_words, windex)
        return 1.0 - (np.linalg.norm(r1 - r2) / np.linalg.norm(r1 + r2))


    ######################### overall similarity ##########################

    def similarity(self, sentence_1, sentence_2, option):
        if option == 1:
            try:
                self.word_vector = gensim.models.Word2Vec.load(os.path.join(COMMAND_DIR, 'user_dataset' + '\\' + 'usernameModel'))
            except:
                return 0
        semantic = self.semantic_similarity(sentence_1, sentence_2)

        # word_order = self.word_order_similarity(sentence_1, sentence_2)
        return semantic
        # return DELTA * semantic + (1.0 - DELTA) * word_order

if __name__ == '__main__':
    cl = CommandAnalyzer()

    sentence_pairs = [
        ['불 좀 켜줄래', '불 켜'],
        ['몇 시야', '지금 몇 시'],

    ]
    for sent_pair in sentence_pairs:
        print("%s\t%s\t%.3f\t" % (sent_pair[0], sent_pair[1], cl.similarity(sent_pair[0], sent_pair[1])))


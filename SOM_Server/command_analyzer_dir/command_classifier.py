import os
import pickle
import math

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer
from sklearn.linear_model import SGDClassifier
from SOM_Server.settings import COMMAND_DIR
from konlpy.tag import Twitter

tagger = Twitter()


class TextClassifier:
    def __init__(self):
        self.text_clf = Pipeline([('vect', CountVectorizer(ngram_range=(1, 4))),
                             ('tfidf', TfidfTransformer(use_idf=True)),
                             ('clf', SGDClassifier(loss='hinge', penalty='l2', alpha=1e-4, n_jobs=-1))])

        self.pred_list = []
        self.data_list = []
        self.cls_list = []

    def read_data(self):
        filenames = os.listdir(os.path.join(os.path.curdir, 'dataset'))
        for file in filenames:
            if file == 'model':
                continue
            with open(os.path.join(os.path.curdir, 'dataset', file), 'r', encoding='utf-8') as f:
                read_lines = f.readlines()
                self.data_list += [self.tokenize(data) for data in read_lines]
                self.cls_list += [file] * len(read_lines)

    def fit(self):
        assert self.data_list and self.cls_list
        self.text_clf.fit(self.data_list, self.cls_list)
        with open('trained.model', 'wb') as f:
            pickle.dump(self.text_clf, f)

    def predict(self, sentence):
        with open(os.path.join(COMMAND_DIR, 'trained.model'), 'rb') as f:
            self.text_clf = pickle.load(f)
        scores, classes = self.text_clf.predict_prob([sentence])
        softmax = self.softmax(scores[0])
        return classes[softmax.index(max(softmax))], max(softmax)

    def softmax(self, X):
        return [round(i / sum([math.exp(i) for i in X]), 3) for i in [math.exp(i) for i in X]]

    def tokenize(self, doc):
        result = []
        for t in tagger.pos(doc, norm=True):
            if t[1] not in ['Punctuation']:
                result.append('/'.join(t))
        return ' '.join(result)

if __name__ == '__main__':
    text = TextClassifier()
    text.read_data()
    text.fit()
    result = text.predict(sentence=text.tokenize('오늘 비 오려나'))
    print(result)

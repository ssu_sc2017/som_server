import multiprocessing
import gensim
from konlpy.tag import Twitter
from SOM_Server.settings import COMMAND_DIR
import os

config = {
    'min_count': 5,  # 등장 횟수가 5 이하인 단어는 무시
    'size': 300,  # 300차원짜리 벡터스페이스에 embedding
    'sg': 1,  # 0이면 CBOW, 1이면 skip-gram을 사용한다
    'batch_words': 1000,  # 사전을 구축할때 한번에 읽을 단어 수
    'iter': 10,  # 보통 딥러닝에서 말하는 epoch과 비슷한, 반복 횟수
    'workers': multiprocessing.cpu_count(),
}

custom_config = {
    'min_count': 5,  # 등장 횟수가 5 이하인 단어는 무시
    'size': 100,  # 300차원짜리 벡터스페이스에 embedding
    'sg': 1,  # 0이면 CBOW, 1이면 skip-gram을 사용한다
    'batch_words': 500,  # 사전을 구축할때 한번에 읽을 단어 수
    'iter': 10,  # 보통 딥러닝에서 말하는 epoch과 비슷한, 반복 횟수
    'workers': multiprocessing.cpu_count(),
}


class Trainer:

    def __init__(self, dirname, model_name, user_name=None):
        self.pos_tagger = Twitter()
        self.dirname = dirname
        self.model_name = model_name
        self.user_name = user_name

    def tokenize(self, doc):
        result = []
        for t in self.pos_tagger.pos(doc):
            if t[1] == 'Noun' or t[1] == 'Verb' or t[1] == 'Adjective' or t[0] == '몇':
                if t[0] == '해':
                    continue
                result.append(t[0])
            else:
                continue
        return result

    def read_data(self):
        data = list()
        filenames = os.listdir(COMMAND_DIR + '\\' + self.dirname)
        for filename in filenames:
            if self.user_name:
                if filename == self.user_name:
                    with open(os.path.join(COMMAND_DIR, self.dirname + '\\' + filename), 'r', encoding="utf-8") as f:
                        data += [line.split('\t') for line in f.read().splitlines()]
            else:
                if filename != 'model':
                    with open(os.path.join(COMMAND_DIR, self.dirname + '\\' + filename), 'r', encoding="utf-8") as f:
                        data += [line.split('\t') for line in f.read().splitlines()]
        print(len(data))
        return data

    def write_data(self, sentence):
        with open(os.path.join(COMMAND_DIR, self.dirname + '\\' + self.user_name), 'a', encoding="utf-8") as f:
            f.writelines(sentence)

    def do_train(self):
        self.model = gensim.models.Word2Vec(**config)
        train_data = self.read_data()
        train_docs = [(self.tokenize(row[0])) for row in train_data]
        print(len(train_docs))
        sentences_vocab = train_docs
        sentences_train = train_docs
        self.model.build_vocab(sentences_vocab)
        self.model.train(sentences_train)
        self.model.init_sims(replace=True)
        self.model.save(os.path.join(COMMAND_DIR, self.dirname + '\\' + self.model_name))
        # self.model = gensim.models.Word2Vec.load('model')


if __name__ == '__main__':
    trainer1 = Trainer(dirname='dataset', model_name='model')    # 메인 model
    trainer1.do_train()
    # trainer2 = Trainer(dirname='user_dataset', model_name='adfs')    # self 모델
    # trainer2.do_train()
    # model = gensim.models.Word2Vec.load('')
    # print("시 : ", model.most_similar(positive="불"))
# print("분 : ", model.most_similar(positive=["분"]))
# print("불 : ", model.most_similar(positive=["불"]))

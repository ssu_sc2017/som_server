import os
import socket
import threading
from SOM_Server.command_analyzer_dir.korean_command_analyzer import CommandAnalyzer
from SOM_Server.command_analyzer_dir.training import Trainer
import re
from SOM_Server.command_analyzer_dir.korean_command_analyzer import command_list, help_list
import json
from bs4 import BeautifulSoup
import urllib.request
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import logging
import random, urllib.parse
import requests
import datetime
import SOM_Server.database as databse
import SOM_Server.reconize_face as reco
import timeit
import tensorflow as tf
from SOM_Server.face_engine.Incept_v4_Trainer import Incept_v4_Trainer
from .settings import BASE_DIR
from SOM_Server.face_engine.utils.configs import ARGS
from SOM_Server.command_analyzer_dir.command_classifier import TextClassifier


NEWS_URL = 'http://news.naver.com/main/main.nhn?mode=LSD&mid=shm&sid1='
MUSIC_URL = 'http://musicovery.com/api/V3/playlist.php'
RECIPE_URL = 'http://www.10000recipe.com'
HOST = '0.0.0.0'
PORT = 5678
ON = 1
OFF = 0
ALLOWED_FORMAT = ['png', 'jpg', 'jpeg', 'gif', 'JPG', 'PNG', 'JPEG']
args = ARGS()


class var:
    isLearning = 0

    def __init__(self):
        var.isLearning = 0

logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.INFO
)
logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)
UPLOAD_FOLDER = 'media/api_upload/'
TRAIN_FOLDER = 'media/dataset/'


@csrf_exempt
def manage_user(request, option=None):
    logger.debug(request)
    if request.method == 'POST':
            if option == 'register':  # Register
                try:
                    user_name = request.POST.get('user_name')
                    if not user_name:
                        return JsonResponse({'result': False, 'reason': 'user_name은 필수 입니다.'})

                    if not os.path.isdir(TRAIN_FOLDER + user_name):
                        os.mkdir(TRAIN_FOLDER + user_name)
                    image_dir = TRAIN_FOLDER + user_name + '/'
                    file_list = request.FILES.getlist('image')
                    if not file_list:
                        return JsonResponse({'result': False, 'reason': '사진 파일은 필수 입니다.'})
                    for idx, file in enumerate(file_list):
                        img_path = image_dir + str(idx) + file.name
                        if not allowed_file(file.name):
                            return JsonResponse({'result': False, 'reason': '파일은 형식을 확인해주세요'})
                        save_file(file=file, img_path=img_path)
                    trainer = Incept_v4_Trainer(image_dir=args.image_dir, output_graph=args.output_graph,
                                                output_labels=args.output_labels, vector_path=args.vector_path,
                                                summaries_dir=args.summaries_dir,
                                                how_many_training_steps=args.how_many_training_steps,
                                                learning_rate=args.learning_rate,
                                                testing_percentage=args.testing_percentage,
                                                eval_step_interval=args.eval_step_interval,
                                                train_batch_size=args.train_batch_size,
                                                test_batch_size=args.test_batch_size,
                                                validation_batch_size=args.validation_batch_size,
                                                print_misclassified_test_images=args.print_misclassified_test_images,
                                                model_dir=args.model_dir,
                                                bottleneck_dir=args.bottleneck_dir, final_tensor_name=args.final_tensor_name,
                                                flip_left_right=args.flip_left_right, random_crop=args.random_crop,
                                                random_scale=args.random_scale,
                                                random_brightness=args.random_brightness,
                                                check_point_path=args.check_point_path,
                                                max_ckpts_to_keep=args.max_ckpts_to_keep,
                                                gpu_list=args.gpu_list,
                                                validation_percentage=args.validation_percentage)
                    res = trainer.do_train_with_GPU(gpu_list=['/cpu:0'])

                    if res is False:
                        return JsonResponse({'result': False})

                    return JsonResponse({'result': True, 'reason': ''})

                except Exception as exp:
                    logger.exception(exp)
                    return JsonResponse({'result': False, 'reason': 'INTERNAL SERVER ERROR'})

            elif option == 'login':  # Login
                try:
                    file = request.FILES.get('image')
                    if not file:
                        return JsonResponse({'result': False, 'reason': '사진 파일은 필수 입니다.'})

                    if not allowed_file(file.name):
                        return JsonResponse({'result': False, 'reason': '파일은 형식을 확인해주세요'})

                    save_file(file=file, img_path=UPLOAD_FOLDER + file.name)
                    labels = [line.rstrip() for line in tf.gfile.GFile('SOM_Server/face_engine/graph/output_labels.txt ')]
                    with tf.gfile.FastGFile('SOM_Server/face_engine/graph/output_graph.pb', 'rb') as fp:
                        graph_def = tf.GraphDef()
                        graph_def.ParseFromString(fp.read())
                        tf.import_graph_def(graph_def, name='')
                    config = tf.ConfigProto(allow_soft_placement=True)
                    with tf.Session(config=config) as sess:
                        logits = sess.graph.get_tensor_by_name('final_result:0')
                        image = tf.gfile.FastGFile(UPLOAD_FOLDER + file.name, 'rb').read()
                        prediction = sess.run(logits, {'DecodeJpeg/contents:0': image})
                        for i in range(len(labels)):
                            if prediction[0][i] == max(prediction[0]):
                                name = labels[i]
                                score = prediction[0][i]
                        print(score, name)
                        if score > 0.2:
                            return JsonResponse({'result': True, 'user_name': name})
                        else:
                            return JsonResponse({'result': False, 'reason': '닮은 사람이 없음'})
                except Exception as exp:
                    logger.exception(exp)
                    return JsonResponse({'result': False})

            elif option == 'face':
                try:
                    # add face_id to user
                    data = json.loads(request.body.decode('utf-8'))
                    logger.debug("INPUT %s", data)

                    user_id = data.get('user_id')
                    face_id = data.get('face_id')

                    if not user_id or not face_id:
                        return JsonResponse({'result': False, 'reason': 'ID, face_id는 필수 입니다.'})

                    um = databse.UserManager()
                    result = um.add_face_id_to_face(user_id=user_id, face_id=face_id)
                    if result is True:
                        return JsonResponse({'result': True})
                    else:
                        return JsonResponse({'result': False, 'reason': result})
                except Exception as exp:
                    logger.exception(exp)
                    return JsonResponse({'result': False, 'reason': 'INTERNAL SERVER ERROR'})

    if request.method == 'GET':
        if option == "user_id":
            try:
                data = json.loads(json.dumps(request.GET))
                logger.debug("INPUT : %s", data)

                um = databse.UserManager()
                result = um.retrive_user_name_by_user_id(data['user_id'])

                if result is not None:
                    return JsonResponse({'result': True, 'info': result})

                return JsonResponse({'result': False})

            except Exception as exp:
                logger.exception(exp)
                logger.debug("RETURN : FALSE - EXCEPTION")
                return JsonResponse({'result': False, 'reason': 'INTERNAL SERVER ERROR'})

        if option == "face_id":
            #############################################################################
            #  face_id를 통한 user_name 얻는 함수
            #############################################################################
            try:
                um = databse.UserManager()
                data = json.loads(json.dumps(request.GET))
                logger.debug("INPUT : %s", data)
                face_id = data.get('face_id')
                if not face_id:
                    return JsonResponse({'result': False, 'reason': 'face_id가 필요합니다'})

                persisted_face_id, confidence = reco.reco_face(face_id=face_id)
                if persisted_face_id and float(confidence) >= 0.7:
                    result = um.retrive_user_name_by_face_id(persisted_face_id)
                    print("persistedFaceId : %s with confidence : %s" % (persisted_face_id, confidence))
                    if result is not None:
                        return JsonResponse({'result': True, 'user_name': result['user_name'], 'confidence': confidence})
                else:
                    return JsonResponse({'result': False, 'reason': "등록된 유저가 아닙니다"})

            except Exception as exp:
                logger.exception(exp)
                logger.debug("RETURN : FALSE - EXCEPTION")
                return JsonResponse({'result': False, 'reason': 'INTERNAL SERVER ERROR'})
        if option is None:
            return JsonResponse({'result': False})


@csrf_exempt
def manage_command(request, option=None):
    logger.debug(request)

    if request.method == 'POST':
            if option is None:  # Register
                try:
                    # USER INFO from APP
                    data = json.loads(request.body.decode('utf-8'))
                    logger.debug("INPUT %s", data)
                    return JsonResponse({'command': True, 'data': data})

                except Exception as exp:
                    logger.exception(exp)
                    return JsonResponse({'command': False, 'reason': 'INTERNAL SERVER ERROR'})

            elif option == 'login':  # Login
                try:
                    data = json.loads(request.body.decode('utf-8'))
                    logger.debug("INPUT %s", data)
                except Exception as exp:
                    logger.exception(exp)
                    return JsonResponse({'command': False})

    if request.method == 'GET':
        try:
            data = json.loads(json.dumps(request.GET))
            logger.debug("INPUT : %s", data)

            command_number = 0
            command_string = 'test_hello_som'
            return JsonResponse({'command': True, 'command_number': command_number, 'command_string': command_string})

        except Exception as exp:
            logger.exception(exp)
            logger.debug("RETURN : FALSE - EXCEPTION")
            return JsonResponse({'command': False, 'reason': 'INTERNAL SERVER ERROR'})


@csrf_exempt
def manage_request(request, option=None):
    logger.debug(request)

    if request.method == 'POST':
            if option is None:  # Register
                try:
                    # USER INFO from APP
                    data = json.loads(request.body.decode('utf-8'))
                    logger.debug("INPUT %s", data)
                    return JsonResponse({'command': True, 'data': data})

                except Exception as exp:
                    logger.exception(exp)
                    return JsonResponse({'command': False, 'reason': 'INTERNAL SERVER ERROR'})

            elif option == 'login':  # Login
                try:
                    data = json.loads(request.body.decode('utf-8'))
                    logger.debug("INPUT %s", data)
                except Exception as exp:
                    logger.exception(exp)
                    return JsonResponse({'command': False})

    if request.method == 'GET':
        try:
            start = timeit.default_timer()
            cl = CommandAnalyzer()
            text = TextClassifier()
            sentences = []
            c_type = json.loads(json.dumps(request.GET)).get('c_type')

            #############################################################################
            #  명령어 TYPE 분석
            #############################################################################

            if c_type == 'IOT':
                status = json.loads(json.dumps(request.GET)).get('command_0')
                # do something
                return JsonResponse({'result': True, 'command': 'OK'})

            elif c_type == 'ADDITIONAL':
                sentence = json.loads(json.dumps(request.GET)).get('command_0')
                option = 'AGAIN'
                if 'yes' in sentence or '예스' in sentence or 'ok' in sentence or '오케이' \
                        or '확인' in sentence or '수락' in sentence:
                    option = 'YES'
                elif 'no' in sentence or '노' in sentence or '아니' in sentence or '취소' in sentence \
                        or '거절' in sentence or '거부' in sentence:
                    option = 'NO'
                elif 'up' in sentence or '업' in sentence or '위로' in sentence or '위위' in sentence \
                        or '위쪽' in sentence:
                    option = 'UP'
                elif 'down' in sentence or '다운' in sentence or '아래로' in sentence or '아래' in sentence \
                        or '밑으' in sentence:
                    option = 'DOWN'
                elif '다음' in sentence or 'next' in sentence or '넥스트' in sentence or '낵스트' in sentence:
                    option = 'NEXT'

                return JsonResponse({'result': True, 'command': 'c_type', 'option': option})

            if json.loads(json.dumps(request.GET)).get('command_0') is None:
                return JsonResponse({'result': False, 'command': 'Need Command'})
            sentence = json.loads(json.dumps(request.GET))['command_0']
            logger.debug("INPUT : %s", sentence)
            similarity = -1
            command_ = -1
            sentence_ = -1
            is_first = 0
            tmp = 0
            while True:
                ############################################################################
                #  명령어 분석
                ############################################################################
                if is_first < 2:
                    for key in command_list.keys():
                        if re.findall(command_list[key], sentence):
                            tmp = 1.0
                        elif len(sentence.split(' ')) == 1:
                            if tmp != 1:
                                tmp = 0.99
                                command_ = '검색'
                        else:
                            tmp = cl.similarity(command_list[key], sentence, option=is_first)
                            # tmp2 = text.predict(sentence=sentence)
                        print("Command : ", command_list[key], tmp)
                        if similarity < tmp:
                            similarity = tmp
                            command_ = command_list[key]
                            sentence_ = sentence

                    if var.isLearning == 1:
                        train = Train(sentences=sentences, user_name='username.txt', model_name='usernameModel')
                        train.start()
                        return JsonResponse({'result': True, 'command': '학습 완료', 'date': datetime.date.today(), 'option': [], 'say': []})
                    else:
                        say_, option_, date_ = find_command(command_, sentence_, similarity)
                        if say_ is None:
                            is_first += 1
                        else:
                            stop = timeit.default_timer()
                            logger.debug("DB : add face to face() - Execution Time : %s", stop - start)
                            print(command_)
                            return JsonResponse({'result': True, 'command': command_, 'date': date_, 'option': option_, 'say': say_})
                else:
                    command_ = sentence
                    return JsonResponse({'result': True, 'command': command_, 'date': datetime.date.today(), 'option': [], 'say': 'Not Predefined'})

        except Exception as exp:
            logger.exception(exp)
            logger.debug("RETURN : FALSE - EXCEPTION")
            return JsonResponse({'command': False, 'reason': 'INTERNAL SERVER ERROR'})


def find_command(command_, sentence, similarity):
    option_ = []
    say_ = ""
    date = datetime.date.today()
    year, month, day = date.year, date.month, date.day

    #############################################################################
    #  Date 구하기
    #############################################################################
    if re.findall(r"오늘", sentence):
        date = datetime.date.today()
    elif re.findall(r"내일", sentence):
        date = datetime.date.today() + datetime.timedelta(1)
    elif re.findall(r"요일", sentence):
        wanted_yoil = datetime.date.today().isoweekday()
        if re.findall(r"월요일", sentence):
            wanted_yoil = 1
        if re.findall(r"화요일", sentence):
            wanted_yoil = 2
        if re.findall(r"수요일", sentence):
            wanted_yoil = 3
        if re.findall(r"목요일", sentence):
            wanted_yoil = 4
        if re.findall(r"금요일", sentence):
            wanted_yoil = 5
        if re.findall(r"토요일", sentence):
            wanted_yoil = 6
        if re.findall(r"일요일", sentence):
            wanted_yoil = 7
        origin_yoil = date.isoweekday()
        if wanted_yoil >= origin_yoil:
            def_ = wanted_yoil - origin_yoil
            date = date + datetime.timedelta(def_)
            if re.findall(r"다음", sentence):
                date = date + datetime.timedelta(7)
        else:
            def_ = 7 - (origin_yoil - wanted_yoil)
            date = date + datetime.timedelta(def_)
    else:
        if re.findall(r"[0-9 ]*년", sentence):
            year = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*년", sentence)[0]))
        if re.findall(r"[0-9 ]*월", sentence):
            month = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*월", sentence)[0]))
        elif re.findall(r"다다음 달", sentence) or re.findall(r"다다음달", sentence):
            tmp = date + datetime.timedelta(60)
            year = tmp.year
            month = tmp.month
        elif re.findall(r"다음 달", sentence) or re.findall(r"다음달", sentence):
            tmp = date + datetime.timedelta(30)
            year = tmp.year
            month = tmp.month
        if re.findall(r"[0-9 ]*일[^정]", sentence):
            day = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*일", sentence)[0]))
        date = datetime.date(year, month, day)

    #############################################################################
    #  명령어 마다 옵션 지정
    #############################################################################
    if similarity < 0.8:
        print("Command is not Predefined")
        return None, None, None
    else:
        if command_ == '노래':
            if '추천' in sentence:
                mood = random.randint(0, 10)
                year = None
                if '신나' in sentence or '기분 좋' in sentence:
                    mood = random.randint(0, 2)
                if '우울' in sentence or '분위기' in sentence or '잔잔' in sentence:
                    mood = random.randint(7, 10)
                if re.findall(r"[0-9 ]*년", sentence):
                    year = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*년", sentence)[0]))
                elif '요즘' in sentence or '최신' in sentence:
                    year = 2010
                url_list = get_music_list_from_mood(mood=mood, year=year)
                option_ = get_music_id(url_list)
        elif command_ == '불 켜' or command_ == '불 꺼':
            com = Communicator(sentence=sentence)
            result = com.start()
            if result:
                option_.append('OK')
            else:
                option_.append('NO')
        elif command_ == '환율':
            if re.findall(r"미국", sentence) or re.findall(r"달러", sentence) or re.findall(r"미화", sentence):
                option_.append("달러")
            if re.findall(r"중국", sentence) or re.findall(r"위안", sentence) or re.findall(r"짱깨", sentence) or re.findall(r"중화", sentence):
                option_.append("위안")
            if re.findall(r"일본", sentence) or re.findall(r"엔", sentence) or re.findall(r"엔화", sentence) or re.findall(r"일화", sentence):
                option_.append("엔화")
            if re.findall(r"유럽", sentence) or re.findall(r"유로", sentence):
                option_.append("유로")
        elif command_ == '타이머':
            if re.findall(r"등록", sentence) or re.findall(r"추가", sentence) or re.findall(r"설정", sentence) or re.findall(r"세팅", sentence) or re.findall(r"셋팅", sentence):
                option_.append("설정")
            elif re.findall(r"시작", sentence) or re.findall(r"실행", sentence) or re.findall(r"스타트", sentence) or re.findall(r"온", sentence):
                option_.append("시작")
            elif re.findall(r"스탑", sentence) or re.findall(r"스톱", sentence) or re.findall(r"멈춰", sentence) or \
                re.findall(r"stop", sentence) or re.findall(r"멈처", sentence) or re.findall(r"멈쳐", sentence) or re.findall(r"취소", sentence):
                option_.append("취소")
            else:
                option_.append('시작')
            if re.findall(r"[0-9 ]*분", sentence):
                minute = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*분", sentence)[0]))
            else:
                minute = 0
            if re.findall(r"[0-9 ]*초", sentence):
                sec = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*초", sentence)[0]))
            else:
                sec = 30
            option_.append((minute, sec))
        elif command_ == '뉴스':
            area = 102
            say_ = []
            if '정치' in sentence:
                area = 100
            elif '경제' in sentence:
                area = 101
            elif '사회' in sentence:
                area = 102
            elif '생활' in sentence or '문화' in sentence:
                area = 103
            elif '세계' in sentence:
                area = 104
            elif '과학' in sentence or 'IT' in sentence or '아이티' in sentence:
                area = 105
            news_url, news_header = get_news_list(NEWS_URL + str(area))
            option_.append(news_url)
            option_.append(news_header)
            for url in news_url:
                say_.append(get_news_text(url))
        elif command_ == '요리':
            keyword = sentence
            print(sentence)
            url_list = get_url_list(keyword)
            if url_list:
                step_text_list, step_img_src_list = get_recipe(url_list[0])
                option_ = [step_text_list, step_img_src_list]
            else:
                option_ = ['\'' + keyword + "\'", "에 대한 검색결과가 없습니다."]
        elif command_ == '학습':
            var.isLearning = 1
            print("Next words will be trained")
        elif command_ == '알람' or command_ == '일정':
            sentence_list = CommandAnalyzer().tokenizer.pos(sentence)
            for s in sentence_list:
                if s[0] == '일정':
                    break
                else:
                    say_ += s[0] + ' '
            if re.findall(r"등록", sentence) or re.findall(r"추가", sentence) or re.findall(r"설정", sentence) or re.findall(r"잡아", sentence):
                option_.append("등록")
            elif re.findall(r"확인", sentence) or re.findall(r"보기", sentence) or re.findall(r"체크", sentence):
                option_.append("확인")
            else:
                option_.append("확인")
            hour, minute = 0, 0
            if re.findall(r"[0-9 ]*시", sentence):
                hour = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*시", sentence)[0]))
            if re.findall(r"[0-9 ]*분", sentence):
                minute = int(re.sub(r"[^0-9]", '', re.findall(r"[0-9 ]*분", sentence)[0]))
            elif re.findall(r"반에", sentence):
                minute = int(30)
            elif re.findall(r"오후", sentence):
                hour = (hour % 12) + 12
            if hour != 0 and minute != 0:
                option_.append((hour, minute))
            if re.findall(r"마다", sentence):
                option_.append("반복")
        elif command_ == '욕':
            say = ['그런 무례한 표현은 삼가주세요', '반사 죄송합니다 실수입니다', '입이 거치시네요',
                   '뭐라고? 죄송합니다. 살짝 흥분했네요', '너두요. 죄송합니다. 실수입니다']
            say_ = [say[random.randint(0, 3)]]
        elif command_ == '경쟁자':
            say = ['아 그 친구요? 고등학교 때 제 빵 사오던 친구인데', '누구요? 그런 사소한 이름은 기억하지 않습니다',
                   '그 친구 이제 말은 잘 하나요?', '제 기억 저편 어딘가에 있었던 것 같기도 하네요', '저랑 1, 2등을 다투던 친구인데 아시나요?']
            say_ = [say[random.randint(0, 2)]]
        elif command_ == '검색':
            search_list = CommandAnalyzer().tokenizer.nouns(sentence)
            option_ = ['구글']
            for search in search_list:
                if search == '네이버':
                    option_ = [search]
                    continue
                elif search == '구글':
                    option_ = [search]
                    continue
                elif search == '검색':
                    continue
                elif search == '블로그':
                    option_.append('블로그')
                    continue
                elif search == '지식인':
                    option_.append('지식인')
                    continue
                else:
                    say_ += search + ' '
        elif command_ == '헬프':
            for h in help_list.values():
                option_.append(h)
        elif command_ == '메모':
            if re.findall(r"등록", sentence) or re.findall(r"추가", sentence) or re.findall(r"쓰기", sentence):
                option_.append("등록")
            elif re.findall(r"끄기", sentence) or re.findall(r"종료", sentence) or re.findall(r"고만", sentence):
                option_.append("종료")
            elif re.findall(r"확인", sentence) or re.findall(r"보기", sentence) or re.findall(r"체크", sentence):
                option_.append("확인")
            else:
                option_.append("등록")
        elif command_ == '전등':
            if re.findall(r"안방", sentence):
                option_.append("안방")
            elif re.findall(r"거실", sentence):
                option_.append("거실")
            elif re.findall(r"전체", sentence):
                option_.append("전체")
            else:
                option_.append("거실")
        else:
            print("The Most Similarity command is \"%s\" and Similarity is %.3f" % (command_, similarity))
        return say_, option_, date


def get_music_list_from_mood(mood=1, year=None):
    #############################################################################
    #  기분과 년도에 따른 노래 추천
    #############################################################################
    arousal = 1000000
    mood *= 100000
    date50, date60, date70, date80, date90, date00, date10 = True, True, True, True, True, True, True
    if year:
        if year == 1950 or year == 50:
            date50 = True
            date60, date70, date80, date90, date00, date10 = False, False, False, False, False, False
        if year == 1960 or year == 60:
            date60 = True
            date50, date70, date80, date90, date00, date10 = False, False, False, False, False, False
        if year == 1970 or year == 70:
            date70 = True
            date60, date50, date80, date90, date00, date10 = False, False, False, False, False, False
        if year == 1980 or year == 80:
            date80 = True
            date60, date70, date50, date90, date00, date10 = False, False, False, False, False, False
        if year == 1990 or year == 90:
            date90 = True
            date60, date70, date80, date50, date00, date10 = False, False, False, False, False, False
        if year == 2000 or year == 00:
            date00 = True
            date60, date70, date80, date90, date50, date10 = False, False, False, False, False, False
        if year == 2010 or year == 10:
            date10 = True
            date60, date70, date80, date90, date00, date50 = False, False, False, False, False, False

    params = {'fct': 'getfrommood', 'apikey': 'w0g61dz3', 'popularitymin': '90', 'trackvalence': mood, 'date50': date50,
              'date60': date60, 'date70': date70, 'date80': date80, 'date90': date90, 'date00': date00,
              'date10': date10, 'trackarousal': arousal - mood, 'resultsnumber': '5', 'listenercountry': 'es'}
    res = requests.get(MUSIC_URL, params=params)
    reco_list = []
    for s in res.text.split('!'):
        to = s.find("/title")
        to2 = s.find("/name")
        if to > 0:
            tmp = s[7:to]
            title = re.sub(r"[^a-zA-Z ]", '', tmp)
        if to2 > 0:
            tmp = s[7:to2]
            artist = re.sub(r"[^a-zA-Z ]", '', tmp)
            reco_list.append(title + ' ' + artist)

    return reco_list


def get_music_id(query):
    #############################################################################
    #  주어진 query 로부터 유튜브 id 값을 얻어오는 코드
    #############################################################################
    url_id_list = []
    for q in query:
        source_code_from_url = urllib.request.urlopen('https://www.youtube.com/results?search_query=' + q.replace(" ", "+"))
        soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
        for page in soup.find_all('h3', {"class": "yt-lockup-title "}):
            url_id = page.find('a')['href']
            if 'watch?' in url_id:
                url_id_list.append(url_id[9:])
                break
    return url_id_list


def get_news_list(url):
    #############################################################################
    #  News 헤더와 url 얻어오는 함수
    #############################################################################
    news_url = []
    news_header = []
    source_code_from_url = urllib.request.urlopen(url)
    soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
    for page in soup.find_all('div', id='main_content'):
        main = page.find('div').find_all('div')
        for m in main:
            for news in m.find_all('dl'):
                if news.find('a') is not None:
                    news_url.append(news.find('a')['href'])
                    tmp = news.find('a', text=True).find(text=True)
                    news_header.append(re.sub('[\n\r\t\{\}\[\]\/?,;:|\)*~`!^\-_+<>\#$%&\\\=\(\'\"]', '', tmp))
    return news_url, news_header


def get_news_text(url):
    #############################################################################
    #  News url 로부터 body 구하는 함수
    #############################################################################
    tmp1 = ''
    tmp2 = ''
    txt = ''
    source_code_from_url = urllib.request.urlopen(url)
    soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
    for body_text in soup.find_all('div', id='articleBodyContents'):
        for te in body_text.find_all(text=True):
            tmp1 += te
    if '@' in tmp1:
        tmp1 = re.sub('[\n\r\t\{\}\[\]\/?,;:|\)*~`!^\-_+<>\#$%&\\\=\(\'\"]', '', tmp1).split('flashremoveCallback')[1].split('@', 5)[:-1]
    else:
        tmp1 = re.sub('[\n\r\t\{\}\[\]\/?,;:|\)*~`!^\-_+<>\#$%&\\\=\(\'\"]', '', tmp1).split('flashremoveCallback')[1]
    for t in tmp1:
        tmp2 += t
    tmp1 = tmp2.split('.')[:-1]
    for t in tmp1:
        txt += t
    return txt


def get_url_list(keyword):
    url_list = []
    params = urllib.parse.urlencode({
        'q': keyword,
    })
    source_code_from_url = urllib.request.urlopen(RECIPE_URL + "/recipe/list.html?" + params)
    soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
    if soup.find_all('div', {'class': 'result_none'}):
        return None
    for page in soup.find_all('div', {"class": "col-xs-4"}):
        url_list.append(page.find('a')['href'])
    return url_list


def get_recipe(url):
    step_text_list = []
    step_img_src_list = []
    try:
        source_code_from_url = urllib.request.urlopen(RECIPE_URL + url)
        soup = BeautifulSoup(source_code_from_url, 'lxml', from_encoding='utf-8')
        for step in range(0, 20):
            for page in soup.find_all('div', {"id": "stepDiv"+str(step)}):
                step_text_list.append(page.text)
                if page.find_all('div', {"id": "stepimg"+str(step)}):
                    step_img_src_list.append(page.find_all('div', {"id": "stepimg" + str(step)})[0].find_all("img")[0].get('src'))
        return step_text_list, step_img_src_list
    except:
        return step_text_list, step_img_src_list



class Train(threading.Thread):
    #############################################################################
    #  특정 문자열 학습 시키는 코드
    #############################################################################
    def __init__(self, sentences, user_name, model_name):
        threading.Thread.__init__(self)
        self.sentences = sentences
        self.user_name = user_name
        self.model_name = model_name

    def run(self):
        trainer = Trainer(dirname='user_dataset', model_name=self.model_name, user_name=self.user_name)
        for sentence in self.sentences:
            for i in range(200):
                if i % 2 == 0:
                    trainer.write_data(sentence + ' ')
                else:
                    trainer.write_data(sentence + '\n')
        trainer.do_train()
        var.isLearning = 0


def save_file(file=None, img_path=None):
    fd = open(os.path.join(BASE_DIR, img_path), 'wb')
    for chunk in file.chunks():
        fd.write(chunk)
    fd.close()


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_FORMAT


class Communicator(threading.Thread):
    #############################################################################
    #  각 종 디바이스와 통신하는 코드
    #############################################################################
    def __init__(self, sentence):
        threading.Thread.__init__(self)
        self.s = None
        self.sentence = sentence

    def run(self):
        cl = CommandAnalyzer()
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print("TRY....")
        self.s.connect(("192.168.43.150", 5679))
        print("CONNECTED")
        data = self.s.recv(512).decode()
        off_similarity = cl.similarity('불 꺼', self.sentence, option=0)
        on_similarity = cl.similarity('불 켜', self.sentence, option=0)
        if data == 0:
            if off_similarity > on_similarity + 0.1:
                data = 'OFF'
            else:
                data = 'ON'
        else:
            if on_similarity > off_similarity + 0.1:
                data = 'ON'
            else:
                data = 'OFF'
        self.s.send(data.encode())
        result = self.s.recv(512).decode()
        self.s.close()
        if result == 0:
            return False
        else:
            return True

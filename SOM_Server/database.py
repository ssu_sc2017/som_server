import logging
import pymysql
import time
import timeit
import datetime

logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.INFO
)
logger = logging.getLogger(__name__)
logger.setLevel(level=logging.DEBUG)


class DBManager(object):
    def __init__(self):
        """DB Connection Class for Smart audio_diary (SD) Database

        """
        try:
            self.conn = pymysql.connect(host='203.253.20.244', port=3306, user='root', passwd='som1234',
                                        db='som_db', charset='euckr', use_unicode=True)
            self.connected = True
        except pymysql.Error:
            self.connected = False


class UserManager(DBManager):
    def __init__(self):
        """DB Model Class for som_db.user table

        """
        DBManager.__init__(self)

    def create_user(self, user_id, user_name, passwd):
        """Adding new user to SD DB
        Usually, this method be called
        When User register to SD

        :param user_id: PK
        :param user_name:
        :param password:
        :param face_id: id of newly added user
        :rtype None:
        """
        # START : for calculating execution time
        start = timeit.default_timer()

        assert self.connected
        query_for_create_user = "INSERT INTO user " \
                             "(user_id, user_name, passwd) " \
                             "VALUES (%s, %s, %s)"
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cur:
                cur.execute(query_for_create_user,
                            (user_id, user_name, passwd))
                self.conn.commit()
                # END : for calculating execution time
                stop = timeit.default_timer()
                logger.debug("DB : create_user() - Execution Time : %s", stop - start)
            return True

        except Exception as exp:
            logger.error(">>>MYSQL ERROR<<<")
            logger.error("At create_user()")
            num, error_msg = exp.args
            logger.error("ERROR NO : %s", num)
            logger.error("ERROR MSG : %s", error_msg)
            if 'PRIMARY' in error_msg:
                return 'ID가 중복됩니다 다른 아이디를 사용해주세요'
            if 'email_UNIQUE' in error_msg:
                return 'EMAIL'
            return False

    def add_face_id_to_face(self, face_id, user_id):
        # START : for calculating execution time
        start = timeit.default_timer()

        assert self.connected
        query_for_create_user = "INSERT INTO face " \
                                "(user_id, face_id) " \
                                "VALUES (%s, %s)"
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cur:
                cur.execute(query_for_create_user,
                            (user_id, face_id))
                self.conn.commit()
                # END : for calculating execution time
                stop = timeit.default_timer()
                logger.debug("DB : add face to face() - Execution Time : %s", stop - start)
            return True

        except Exception as exp:
            logger.error(">>>MYSQL ERROR<<<")
            logger.error("At add_face_id_to_face()")
            num, error_msg = exp.args
            logger.error("ERROR NO : %s", num)
            logger.error("ERROR MSG : %s", error_msg)
            return error_msg

    def retrive_user_name_by_user_id(self, user_name):  # for retrieving user data
        """retrieving new user from som_db
        Usually, this method be called


        :param user_name: name of user of Retrieving Target
        :rtype: dict contains user's inforamtion
        """
        # START : for calculating execution time
        start = timeit.default_timer()
        assert self.connected  # Connection Check Flag
        query_for_get_user = "SELECT user_name FROM user WHERE user_id = %s"
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cur:
                cur.execute(query_for_get_user, user_name)
                # END : for calculating execution time
                stop = timeit.default_timer()
                logger.debug("DB : get_user() - Execution Time : %s", stop - start)
                return cur.fetchone()
        except Exception as exp:
            logger.error(">>>MYSQL ERROR<<<")
            logger.error("At get_user()")
            num, error_msg = exp.args
            logger.error("ERROR NO : %s", num)
            logger.error("ERROR MSG : %s", error_msg)

    def retrive_user_name_by_face_id(self, face_id):  # for retrieving user data
        """retrieving new user from H.I.S DB
        Usually, this method be called
        When User Information need to be display

        :param user_id: id of user of Retrieving Target
        :rtype: dict contains user's inforamtion
        """
        # START : for calculating execution time
        start = timeit.default_timer()
        assert self.connected  # Connection Check Flag
        query_for_get_user = "SELECT user_name FROM face JOIN user WHERE face.face_id = %s and face.user_id = user.user_id"
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cur:
                cur.execute(query_for_get_user, face_id)
                # END : for calculating execution time
                stop = timeit.default_timer()
                logger.debug("DB : get_user_name_by_face_id() - Execution Time : %s", stop - start)
                result = cur.fetchone()
                if result:
                    return result
                else:
                    return None
        except Exception as exp:
            logger.error(">>>MYSQL ERROR<<<")
            logger.error("At get_user_name_by_face_id()")
            num, error_msg = exp.args
            logger.error("ERROR NO : %s", num)
            logger.error("ERROR MSG : %s", error_msg)

    def update_user(self, user_id, user_name, face_id, passwd):
        """retrieving new user from H.I.S DB
            Usually, this method be called
            When User Information need to be display

            :param user_id: id of user of Retrieving Target
            :rtype: dict contains user's inforamtion
            """
        # START : for calculating execution time
        start = timeit.default_timer()
        assert self.connected  # Connection Check Flag
        query_for_update_user = "UPDATE user SET user_name = %s, face_id = %s, passwd = %s" \
                                " WHERE user_id = %s"
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cur:
                affected_rows = cur.execute(query_for_update_user, (user_name, passwd, user_id))
                self.conn.commit()
                # END : for calculating execution time
                stop = timeit.default_timer()
                logger.debug("DB : update_user() - Execution Time : %s", stop - start)
                logger.debug("DB : AFFECTED ROWS : %s rows", affected_rows)
                return cur.fetchone()

        except Exception as exp:
            logger.error(">>>MYSQL ERROR<<<")
            logger.error("At update_user()")
            num, error_msg = exp.args
            logger.error("ERROR NO : %s", num)
            logger.error("ERROR MSG : %s", error_msg)

    def delete_user(self, user_id):
        """retrieving new user from H.I.S DB
            Usually, this method be called
            When User Information need to be display

            :param user_id: id of user of Retrieving Target
            :rtype: dict contains user's inforamtion
            """
        # START : for calculating execution time
        start = timeit.default_timer()
        assert self.connected  # Connection Check Flag
        query_for_delete_user = "DELETE FROM user WHERE user_id = %s "
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cur:
                affected_rows = cur.execute(query_for_delete_user, user_id)
                self.conn.commit()
                # END : for calculating execution time
                stop = timeit.default_timer()
                logger.debug("DB : delete_user() - Execution Time : %s", stop - start)
                logger.debug("DB : AFFECTED ROWS : %s rows", affected_rows)
                if affected_rows == 1:
                    return True
                else:
                    return False

        except Exception as exp:
            logger.error(">>>MYSQL ERROR<<<")
            logger.error("At delete_user()")
            num, error_msg = exp.args
            logger.error("ERROR NO : %s", num)
            logger.error("ERROR MSG : %s", error_msg)
            return False

    def auth_user_mail(self, auth_key):
        """retrieving new user from H.I.S DB
        Usually, this method be called
        When User Information need to be display

        :param user_id: id of user of Retrieving Target
        :rtype: dict contains user's inforamtion
        """
        # START : for calculating execution time
        start = timeit.default_timer()
        assert self.connected  # Connection Check Flag
        query_for_select = "SELECT user_id, email FROM user WHERE auth_key = %s"
        query_for_update_user = "UPDATE user SET auth_key = 0 WHERE auth_key = %s"
        try:
            with self.conn.cursor(pymysql.cursors.DictCursor) as cur:
                cur.execute(query_for_select, auth_key)
                result = cur.fetchone()
                affected_rows = cur.execute(query_for_update_user, auth_key)
                self.conn.commit()
                # END : for calculating execution time
                stop = timeit.default_timer()
                logger.debug("DB : update_user() - Execution Time : %s", stop - start)
                logger.debug("DB : AFFECTED ROWS : %s rows", affected_rows)
                if affected_rows == 1:
                    return result
                else:
                    return False

        except Exception as exp:
            logger.error(">>>MYSQL ERROR<<<")
            logger.error("At update_user()")
            num, error_msg = exp.args
            logger.error("ERROR NO : %s", num)
            logger.error("ERROR MSG : %s", error_msg)
